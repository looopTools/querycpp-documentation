# Introduction 

Welcome to the documentation for QueryC++. 
This documentation is always relevant for the latest version of QueryC++.

|Information  | Value       |
|:------------|-------------:|
|Version     | 3.0.0       | 
|C++ Standard| 17          |
| License    | [BSD 3-Clause](https://gitlab.com/looopTools/querycpp/-/blob/main/LICENSE) |
|Maintainer  | Lars Nielsen |

## What is QueryC++ 

QueryC++ (a.k.a querycpp) is a SQL query build for modern C++ and is indented for usage with libraries such as [libpqxx](https://github.com/jtv/libpqxx). With QueryC++ being the query builder and libpqxx handling the database connection and query execution. 

The goal of QueryC++ is to provide an easy and simple way to construct SQL queries in C++ and remove the needed for using strings for this purpose in C++. 
This is both to increase maintainability and readability of source code, but also to decrease the potential of having run time syntax errors in queries. 

## What is QueryC++ not

QueryC++ is not an Object Relational Mapping (ORM) framework. 
The responsibilities and features of a ORM is beyond the intended purpose of QueryC++.

# Document Structure 

[Building and Usage](./building.md) provides a explanation for how to build and use QueryC++ in your project. 

[Example](./example.md) presents an example for using QueryC++ in collaboration with [libpqxx](https://github.com/jtv/libpqxx) to execute SQL queries on a Postgresql database.
- [CHANGELOG](./changelog.md)
- [Contributing](./contributing.md)
- [Contributors](./contributors.md)
